#!/bin/bash

s1=$(kubectl get deployments)
BACKEND_IMAGE="osama93/devops_backend_app:${BUILD_NUMBER}"
MIGRATION_IMAGE="osama93/devops_migration:${BUILD_NUMBER}"

if [ "$s1" == "" ] ; then
   kubectl create -f /home/emumba/eMumba/DevOps/devops_exercise_curriculum/deployment_postgres.yaml
   sleep 10
   kubectl create -f /home/emumba/eMumba/DevOps/devops_exercise_curriculum/devops_migration/deployment_migration_jenkins.yaml
   sleep 10
   kubectl create -f /home/emumba/eMumba/DevOps/devops_exercise_curriculum/devops_backend_app/deployment_backend_jenkins.yaml
elif [ "$s1" != "" ] ; then
   kubectl set image deployment/migration-deployment migration-con1=$MIGRATION_IMAGE 
   kubectl set image deployment/backend-deployment backend-con1=$BACKEND_IMAGE
fi





